package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/admin_alterMessage_Servlet")
public class admin_alterMessage_Servlet extends HttpServlet {
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean adminStatus = false;
        boolean teacherStatus = false;
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessage")) {
                HttpSession session = request.getSession();
                Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    adminStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!adminStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        //根据教师id查询教师信息--------------------------------------------------------------------------------
        String queryStuId = request.getParameter("queryStuId");
        if (queryStuId != null) {
            IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
            Student stuId = iStudentMapper.findStuId(Integer.parseInt(queryStuId));

            //如果查询的账号不存在
            if (stuId == null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }

            String queryMes = JSON.toJSONString(stuId);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(queryMes);
            out.flush();
            out.close();
            return;
        }

        //根据教师账号查询教师信息--------------------------------------------------------------------------------
        String queryStuAccount = request.getParameter("queryStuAccount");
        if (queryStuAccount != null) {

            IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
            Student stuAcc = iStudentMapper.findAccount(queryStuAccount);

            //如果查询的账号不存在
            if (stuAcc == null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }

            String queryMes = JSON.toJSONString(stuAcc);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(queryMes);
            out.flush();
            out.close();
            return;
        }

        //保存更改后的教师信息--------------------------------------------------------------------------------
        String alterStudentMessage = request.getParameter("alterStudentMessage");
        if (alterStudentMessage != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            Student studentMes = JSON.parseObject(alterStudentMessage, Student.class);
            iAdminMapper.updateStudent(studentMes);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("alterSuccess");
            out.flush();
            out.close();
            return;
        }

        //根据教师id查询教师信息--------------------------------------------------------------------------------
        String queryTeaId = request.getParameter("queryTeaId");
        if (queryTeaId != null) {

            ITeacherMapper iTeacherMapper = (ITeacherMapper) Tools.getImp("ITeacherMapper");
            Teacher tea = iTeacherMapper.findTeaId(Integer.parseInt(queryTeaId));

            //如果查询的账号不存在
            if (tea == null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }

            String queryMes = JSON.toJSONString(tea);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(queryMes);
            out.flush();
            out.close();
            return;
        }

        //根据教师账号查询教师信息--------------------------------------------------------------------------------
        String queryTeaAccount = request.getParameter("queryTeaAccount");
        if (queryTeaAccount != null) {

            ITeacherMapper iTeacherMapper = (ITeacherMapper) Tools.getImp("ITeacherMapper");
            Teacher tea = iTeacherMapper.findAccount(queryTeaAccount);

            //如果查询的账号不存在
            if (tea == null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }

            String queryMes = JSON.toJSONString(tea);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(queryMes);
            out.flush();
            out.close();
            return;
        }

        //保存更改后的教师信息--------------------------------------------------------------------------------
        String alterTeacherMessage = request.getParameter("alterTeacherMessage");
        if (alterTeacherMessage != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            Teacher teacher = JSON.parseObject(alterTeacherMessage, Teacher.class);
            iAdminMapper.updateTeacher(teacher);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("alterSuccess");
            out.flush();
            out.close();
            return;
        }
    }

    public void destroy() {
    }
}
package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.utils.Tools;

import java.io.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/student_logIn_Servlet")
public class student_logIn_Servlet extends HttpServlet {

    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //判断是否要自动登录
        String str = request.getParameter("stuAutoLogIn");
        //如果客户端存在cookie
        if (str != null && str.equals("YES")) {
            Cookie[] cookies = request.getCookies();
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessageStudent")) {
                    HttpSession session = request.getSession();
                    Student attribute = (Student) session.getAttribute(tCookie.getValue());
                    if (attribute != null) {
                        response.setContentType("text/html;charset=UTF-8");
                        PrintWriter out = response.getWriter();
                        out.write("/html/studentHome.html");
                        out.flush();
                        out.close();
                        return;
                    }
                }
            }
            PrintWriter out = response.getWriter();
            out.write("NoAutoLogin");
            out.flush();
            out.close();
            return;
        }

        //获取参数
        String content = request.getParameter("studentJson");
        Student student = JSON.parseObject(content, Student.class);

        //获取操作接口
        IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
        Student query = iStudentMapper.findAccount(student.getStuAccount());
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        if (query == null) {
            out.write("AccountDoesNotExist");
            out.flush();
            out.close();
            return;
        } else if (!query.getStuPassword().equals(student.getStuPassword())) {
            out.write("PasswordError");
            out.flush();
            out.close();
            return;
        }

        //发送要跳转的页面的url
        out.write("/html/studentHome.html");

        //根据时间产生的用户标识
        String userId = "" + (new Date().getTime());

        //cookie存储key:accountMessage,value:根据时间毫秒数产生的id
        Cookie cookie = new Cookie("accountMessageStudent", userId);
        cookie.setPath("/");
        //在三小时内有效
        cookie.setMaxAge(60 * 60 * 3);
        response.addCookie(cookie);

        //把时间产生的用户id设置为key,账户信息设置为value
        HttpSession session = request.getSession();
        session.setAttribute(userId, query);

        out.flush();
        out.close();
    }

    public void destroy() {

    }
}
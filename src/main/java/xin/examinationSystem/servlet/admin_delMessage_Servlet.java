package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/admin_delMessage_Servlet")
public class admin_delMessage_Servlet extends HttpServlet {
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean adminStatus = false;
        boolean teacherStatus = false;
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessage")) {
                HttpSession session = request.getSession();
                Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    adminStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!adminStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        String delStudentIdSet = request.getParameter("delStudentIdSet");
        if (delStudentIdSet != null) {
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            String[] split = delStudentIdSet.split("-");
            iAdminMapper.removeStudentSet(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("OK");
            out.flush();
            out.close();
            return;
        }

        String delStudentId = request.getParameter("delStudentId");
        if (delStudentId != null) {
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            if (iStudentMapper.findStuId(Integer.parseInt(delStudentId)) != null) {
                iAdminMapper.removeStudent(Integer.parseInt(delStudentId));
                out.write("OK");
            } else {
                out.write("cannotFind");
            }
            out.flush();
            out.close();
            return;
        }

        String delTeacherIdSet = request.getParameter("delTeacherIdSet");
        if (delTeacherIdSet != null) {
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            String[] split = delTeacherIdSet.split("-");
            iAdminMapper.removeTeacherSet(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("OK");
            out.flush();
            out.close();
            return;
        }

        String delTeacherId = request.getParameter("delTeacherId");
        if (delTeacherId != null) {
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            ITeacherMapper iTeacherMapper = (ITeacherMapper) Tools.getImp("ITeacherMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            if (iTeacherMapper.findTeaId(Integer.parseInt(delTeacherId)) != null) {
                iAdminMapper.removeTeacher(Integer.parseInt(delTeacherId));
                out.write("OK");
            } else {
                out.write("cannotFind");
            }
            out.flush();
            out.close();
            return;
        }
    }

    public void destroy() {
    }
}
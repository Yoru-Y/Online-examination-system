package xin.examinationSystem.account.testPaper;

/**
 * 试卷标题类
 */
public class TP_Title {
    private Integer tpId;
    private String headline;
    private String subtitle;

    public Integer getTpId() {
        return tpId;
    }

    public TP_Title(Integer tpId, String headline, String subtitle) {
        this.tpId = tpId;
        this.headline = headline;
        this.subtitle = subtitle;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public String toString() {
        return "TP_Title{" +
                "tpId=" + tpId +
                ", headline='" + headline + '\'' +
                ", subtitle='" + subtitle + '\'' +
                '}';
    }
}

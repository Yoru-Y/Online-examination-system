package xin.examinationSystem.account.testPaper;

import java.util.Date;

public class TestPaper {
    /**
     * 题目编号
     */
    private Integer id;

    /**
     * 题目内容
     */
    private String node;

    /**
     * 开始考试时间
     */

    private Date startTime;

    /**
     * 结束考试时间
     */

    private Date entTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEntTime() {
        return entTime;
    }

    public void setEntTime(Date entTime) {
        this.entTime = entTime;
    }

    @Override
    public String toString() {
        return "TestPaper{" +
                "id=" + id +
                ", node='" + node + '\'' +
                ", startTime=" + startTime +
                ", entTime=" + entTime +
                '}';
    }
}

package xin.examinationSystem.account.testPaper;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class TestPaperSet {
    private Integer tpId;
    private String headline;
    private String subtitle;
    private Date startTime;

    //用来判断这场考试的试卷是否已经全部批改完成,为true时标识还需要批改
    @JSONField(format="yyyy-MM-dd HH时")
    private Boolean status = false;

    public TestPaperSet() {
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TestPaperSet{" +
                "tpId=" + tpId +
                ", headline='" + headline + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", startTime=" + startTime +
                ", status=" + status +
                '}';
    }
}

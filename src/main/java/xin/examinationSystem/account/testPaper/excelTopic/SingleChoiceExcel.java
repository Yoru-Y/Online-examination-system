package xin.examinationSystem.account.testPaper.excelTopic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 单选题类
 */
@Data
public class SingleChoiceExcel extends ExcelClass {
    /**
     * 题目编号,@ExcelIgnore忽略这个字段
     */
    private Integer id;

    /**
     * 题目内容
     */
    @ExcelProperty("题目")
    private String topic;

    /**
     * 题目答案
     */
    @ExcelProperty("正确选项(1,2,3,4)")
    private String answer;

    /**
     * 题目选项A,B,C,D
     */
    @ExcelProperty("选项A")
    private String option_A;

    @ExcelProperty("选项B")
    private String option_B;

    @ExcelProperty("选项C")
    private String option_C;

    @ExcelProperty("选项D")
    private String option_D;

    public SingleChoiceExcel() {
        super.topicId = TopicId.SingleChoiceExcel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOption_A() {
        return option_A;
    }

    public void setOption_A(String option_A) {
        this.option_A = option_A;
    }

    public String getOption_B() {
        return option_B;
    }

    public void setOption_B(String option_B) {
        this.option_B = option_B;
    }

    public String getOption_C() {
        return option_C;
    }

    public void setOption_C(String option_C) {
        this.option_C = option_C;
    }

    public String getOption_D() {
        return option_D;
    }

    public void setOption_D(String option_D) {
        this.option_D = option_D;
    }

    @Override
    public String toString() {
        return "SingleChoice{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                ", option_A='" + option_A + '\'' +
                ", option_B='" + option_B + '\'' +
                ", option_C='" + option_C + '\'' +
                ", option_D='" + option_D + '\'' +
                '}';
    }
}

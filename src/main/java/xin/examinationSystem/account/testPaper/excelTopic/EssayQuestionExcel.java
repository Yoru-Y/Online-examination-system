package xin.examinationSystem.account.testPaper.excelTopic;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;

public class EssayQuestionExcel extends ExcelClass {
    /**
     * 题目编号
     */
    @ExcelIgnore
    private Integer id;

    /**
     *题目内容
     */
    @ExcelProperty("题目")
    private String topic;

    /**
     *题目答案
     */
    @ExcelIgnore
    private String answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "EssayQuestion{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}

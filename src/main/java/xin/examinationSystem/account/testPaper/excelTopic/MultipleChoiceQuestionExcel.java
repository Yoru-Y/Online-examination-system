package xin.examinationSystem.account.testPaper.excelTopic;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 多选题类
 */
@Data
public class MultipleChoiceQuestionExcel extends ExcelClass {
    /**
     * 题目编号
     */

    @ExcelIgnore
    private Integer id;

    /**
     * 题目内容
     */
    @ExcelProperty("题目")
    private String topic;

    /**
     * 题目答案
     */
    @ExcelProperty("正确选项(正确为小写t,错误为小写f)")
    private String answer;

    /**
     * 题目选项A,B,C,D
     */
    @ExcelProperty("选项A")
    private String option_A;

    @ExcelProperty("选项B")
    private String option_B;

    @ExcelProperty("选项C")
    private String option_C;

    @ExcelProperty("选项D")
    private String option_D;

}

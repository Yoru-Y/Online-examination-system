package xin.examinationSystem.account;

import java.io.Serializable;

public class Admin implements Serializable {
    private Integer admId;
    private String admName;
    private String admAccount;
    private String admPassword;
    private String admBriefIntroduction;

    public Admin() {

    }

    public Integer getAdmId() {
        return admId;
    }

    public void setAdmId(Integer admId) {
        this.admId = admId;
    }

    public String getAdmName() {
        return admName;
    }

    public void setAdmName(String admName) {
        this.admName = admName;
    }

    public String getAdmAccount() {
        return admAccount;
    }

    public void setAdmAccount(String admAccount) {
        this.admAccount = admAccount;
    }

    public String getAdmPassword() {
        return admPassword;
    }

    public void setAdmPassword(String admPassword) {
        this.admPassword = admPassword;
    }

    public String getAdmBriefIntroduction() {
        return admBriefIntroduction;
    }

    public void setAdmBriefIntroduction(String admBriefIntroduction) {
        this.admBriefIntroduction = admBriefIntroduction;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "admId=" + admId +
                ", admName='" + admName + '\'' +
                ", admAccount='" + admAccount + '\'' +
                ", admPassword='" + admPassword + '\'' +
                ", admBriefIntroduction='" + admBriefIntroduction + '\'' +
                '}';
    }
}

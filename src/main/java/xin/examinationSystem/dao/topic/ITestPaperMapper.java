package xin.examinationSystem.dao.topic;

import xin.examinationSystem.account.testPaper.*;
import xin.examinationSystem.account.testPaper.topic.*;

/**
 * 对试卷进行操作
 */
public interface ITestPaperMapper {
    /**
     * 获取问答题的数量
     *
     * @return 返回数据库中问答题的数量
     */
    Integer getEssayQuestionNum();

    /**
     * 获取填空题的数量
     *
     * @return 返回数据库中填空题的数量
     */
    Integer getGapFillingNum();

    /**
     * 获取判断题的数量
     *
     * @return 返回数据库中判断题的数量
     */
    Integer getJudgeQuestionNum();

    /**
     * 获取多选题的数量
     *
     * @return 返回数据库中多选题的数量
     */
    Integer getMultipleChoiceQuestionNum();

    /**
     * 获取单选题的数量
     *
     * @return 返回数据库中单选题的数量
     */
    Integer getSingleChoiceNum();

    /**
     * 添加单选题
     *
     * @param sc 添加的内容
     */
    void addSingleChoice(SingleChoice sc);

    /**
     * 添加多选题
     *
     * @param mcq 添加的内容
     */
    void addMultipleChoiceQuestion(MultipleChoiceQuestion mcq);

    /**
     * 添加判断题
     *
     * @param jq 添加的内容
     */
    void addJudgeQuestion(JudgeQuestion jq);

    /**
     * 添加填空题
     *
     * @param gf 添加的内容
     */
    void addGapFilling(GapFilling gf);

    /**
     * 添加简答题
     *
     * @param eq 添加的内容
     */
    void addEssayQuestion(EssayQuestion eq);


    /**
     * 添加试卷信息
     *
     * @param tp 添加的内容
     */
    void addTestPaper(TestPaper tp);

    SingleChoice[] queryAllSingleChoice();

    MultipleChoiceQuestion[] queryAllMultipleChoiceQuestion();

    JudgeQuestion[] queryAllJudgeQuestion();

    GapFilling[] queryAllGapFilling();

    EssayQuestion[] queryAllEssayQuestion();

    void delSingleChoice(Integer id);

    void delMultipleChoiceQuestion(Integer id);

    void delJudgeQuestion(Integer id);

    void delGapFilling(Integer id);

    void delEssayQuestion(Integer id);

    SingleChoice querySingleChoice(Integer id);

    MultipleChoiceQuestion queryMultipleChoiceQuestion(Integer id);

    JudgeQuestion queryJudgeQuestion(Integer id);

    GapFilling queryGapFilling(Integer id);

    EssayQuestion queryEssayQuestion(Integer id);

    TestPaper getCurTestPaper();

    void addStudent_test_paper(Student_test_paper stp);

    Student_test_paper[] query_student_test_paper_stuId_and_tpId(Integer stuId, Integer tpId);

    void addLocation_table(Location_table lt);

    Location_table query_location_table(Location_table lt);

    Student_test_paper[] query_all_Student_test_paper(Integer id);

    TestPaperGrate[] queryTestPaperGrate(Integer id);

    void add_test_paper_grate(TestPaperGrate tpg);

    void update_test_paper_grate(TestPaperGrate tpg);

    void add_TP_title(TP_Title t);

    TP_Title query_TP_title(Integer id);

    void set_TP_grade(Integer tpId, Integer grade, Integer stuId);

    Student_test_paper[] query_not_correct_TP(Integer tpId);

    Student_test_paper[] query_student_topic_t(Integer tpId, Integer stuId);

    TestPaperSet[] query_all_tpSet();

    StuLookTestPaperSet[] query_stu_all_tpSet(Integer stuId);

    Integer tp_whether_check_accomplish(Integer id);

}

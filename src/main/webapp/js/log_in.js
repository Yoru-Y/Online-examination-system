/*登录表单中email与password的字体动画*/
$(function () {
    const labels = document.querySelectorAll(".form-control label")
    labels.forEach(label => {
        label.innerHTML = label.innerText.split('').map((letter, index) => `<span style="transition-delay:${index * 35}ms">${letter}</span>`).join('');
    });

    function getCookie(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i].trim();
            if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
        }
        return "";
    }
});

//禁止页面缩放，ctrl+滚轮，无法禁止
$(function () {
    $(document).keydown(function (event) {
        if ((event.ctrlKey === true || event.metaKey === true) && (event.which === 189 || event.which === 187 || event.which === 173 || event.which === 61 || event.which === 107 || event.which === 109))
        {
            event.preventDefault();
        }
    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
        if (event.ctrlKey === true || event.metaKey) {
            event.preventDefault();
        }
    });
})

$(function () {
    //当前选中的身份盒子编号,从左到右0到2
    let curPitchOn = 2;   //默认选中学生

    function studentClass() {
        this.stuId = 0;
        this.stuName = "";
        this.stuAccount = "";
        this.stuPassword = "";
        this.stuBriefIntroduction = "";
    }

    function teacherClass() {
        this.teaId = 0;
        this.teaName = "";
        this.teaAccount = "";
        this.teaPassword = "";
        this.teaBriefIntroduction = "";
    }

    function adminClass() {
        this.admId = 0;
        this.admName = "";
        this.admAccount = "";
        this.admPassword = "";
        this.admBriefIntroduction = "";
    }

    $.ajax({
        url: "/admin_logIn_Servlet",
        type: "post",
        data: {"admAutoLogIn": "YES"},
        success: function (resultStr) {
            if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                window.location.href = resultStr;
            }
        }
    });

    $.ajax({
        url: "/teacher_logIn_Servlet",
        type: "post",
        data: {"teaAutoLogIn": "YES"},
        success: function (resultStr) {
            if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                window.location.href = resultStr;
            }
        }
    });

    $.ajax({
        url: "/student_logIn_Servlet",
        type: "post",
        data: {"stuAutoLogIn": "YES"},
        success: function (resultStr) {
            if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                window.location.href = resultStr;
            }
        }
    });

    //获取到所有lable
    let labelSet = $("#identity label");
    labelSet.css("color", "#d4d4d4");
    $("#identity").children().eq(curPitchOn).css("color", "rgb(0, 129, 255)");

    labelSet.click(function () {
        //获取选中的子元素编号
        let eleIndex = $("#identity label").index(this);
        curPitchOn = eleIndex;
        if (eleIndex === 0) {    //管理员字体颜色
            labelSet.css("color", "#d4d4d4");
            $("#identity").children().eq(curPitchOn).css("color", "rgb(255, 128, 64)");

            $.ajax({
                url: "/admin_logIn_Servlet",
                type: "post",
                data: {"admAutoLogIn": "YES"},
                success: function (resultStr) {
                    if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                        window.location.href = resultStr;
                    }
                }
            });
        } else if (eleIndex === 1) {   //教师字体颜色
            labelSet.css("color", "#d4d4d4");
            $("#identity").children().eq(curPitchOn).css("color", "rgb(251, 0, 251)");
            $.ajax({
                url: "/teacher_logIn_Servlet",
                type: "post",
                data: {"teaAutoLogIn": "YES"},
                success: function (resultStr) {
                    if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                        window.location.href = resultStr;
                    }
                }
            });
        } else {   //学生字体颜色
            labelSet.css("color", "#d4d4d4");
            $("#identity").children().eq(curPitchOn).css("color", "rgb(0, 129, 255)");
            $.ajax({
                url: "/student_logIn_Servlet",
                type: "post",
                data: {"stuAutoLogIn": "YES"},
                success: function (resultStr) {
                    if (resultStr !== "AccountDoesNotExist" && resultStr !== "PasswordError" && resultStr !== "NoAutoLogin") {
                        window.location.href = resultStr;
                    }
                }
            });
        }
    });

    //登录页面的登录按钮
    $("#log_in_btn").click(function () {
        if (curPitchOn === 2) {
            let student = new studentClass();
            student.stuAccount = $("#logInUserName").val();
            student.stuPassword = $("#logInUserPass").val();
            //转换为json格式
            let studentJSON = JSON.stringify(student);
            $.ajax({
                url: "student_logIn_Servlet",
                type: "post",
                data: {"studentJson": studentJSON},
                success: function (resultStr) {
                    if (resultStr === "AccountDoesNotExist") {
                        alert("账号不存在");
                    } else if (resultStr === "PasswordError") {
                        alert("密码错误");
                    } else {
                        //跳转到主界面
                        window.location.href = resultStr;
                    }
                },
                error() {
                    alert("服务器连接失败");
                }
            });
        } else if (curPitchOn === 1) {
            let teacher = new teacherClass();
            teacher.teaAccount = $("#logInUserName").val();
            teacher.teaPassword = $("#logInUserPass").val();
            //转换为json格式
            let teacherJSON = JSON.stringify(teacher);
            $.ajax({
                url: "teacher_logIn_Servlet",
                type: "post",
                data: {"teacherJson": teacherJSON},
                success: function (resultStr) {
                    if (resultStr === "AccountDoesNotExist") {
                        alert("账号不存在");
                    } else if (resultStr === "PasswordError") {
                        alert("密码错误");
                    } else {
                        //跳转到主界面
                        window.location.href = resultStr;
                    }
                },
                error() {
                    alert("服务器连接失败");
                }
            });
        } else {
            let admin = new adminClass();
            admin.admAccount = $("#logInUserName").val();
            admin.admPassword = $("#logInUserPass").val();
            //转换为json格式
            let adminJSON = JSON.stringify(admin);
            $.ajax({
                url: "admin_logIn_Servlet",
                type: "post",
                data: {"adminJson": adminJSON},
                success: function (resultStr) {
                    if (resultStr === "AccountDoesNotExist") {
                        alert("账号不存在");
                    } else if (resultStr === "PasswordError") {
                        alert("密码错误");
                    } else {
                        //跳转到主界面
                        window.location.href = resultStr;
                    }
                },
                error() {
                    alert("服务器连接失败");
                }
            });
        }
    });
});
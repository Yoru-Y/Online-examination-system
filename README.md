### 在线考试系统


#### 介绍
开源的在线考试系统，希望能帮到需要的人，会持续更新的。

#### 简介
太多了讲不完，看截图吧，大家试试看就知道了，简单好用，功能特全的。

#### 安装环境

JDK8以上版本
MySQL8以上版本
Tomcat8以上版本

#### 使用说明

管理员身份可以CRUD教师与学生信息。
教师身份可以CRUD学生信息与题目信息。
学生身份可以参加考试和浏览历史考试信息。

#### 截图
### 管理员身份
![登录页面](https://images.gitee.com/uploads/images/2022/0212/164524_68052d76_10390001.png "屏幕截图.png")
![管理员浏览信息页面](https://images.gitee.com/uploads/images/2022/0212/164722_80235cb4_10390001.png "屏幕截图.png")
![管理员添加信息页面](https://images.gitee.com/uploads/images/2022/0212/164759_d203bc8c_10390001.png "屏幕截图.png")
![管理员修改信息页面](https://images.gitee.com/uploads/images/2022/0212/164924_741c945c_10390001.png "屏幕截图.png")
![管理员删除信息页面](https://images.gitee.com/uploads/images/2022/0212/165016_199d2a40_10390001.png "屏幕截图.png")
![教师批改试卷页面](https://images.gitee.com/uploads/images/2022/0212/165138_56ef9e1e_10390001.png "屏幕截图.png")
![教师批改试卷页面](https://images.gitee.com/uploads/images/2022/0212/165211_1c04fca3_10390001.png "屏幕截图.png")
![教师批改试卷页面](https://images.gitee.com/uploads/images/2022/0212/165326_148e198f_10390001.png "屏幕截图.png")
![教师批改试卷页面](https://images.gitee.com/uploads/images/2022/0212/165354_0d940d77_10390001.png "屏幕截图.png")
![教师添加试卷页面](https://images.gitee.com/uploads/images/2022/0212/165431_215b05be_10390001.png "屏幕截图.png")
![教师查询题目页面](https://images.gitee.com/uploads/images/2022/0212/165504_c357e473_10390001.png "屏幕截图.png")
![教师添加题目信息](https://images.gitee.com/uploads/images/2022/0212/165620_f235a4c9_10390001.png "屏幕截图.png")
![教师批量导入信息](https://images.gitee.com/uploads/images/2022/0212/165713_3d5c4b89_10390001.png "屏幕截图.png")
![学生开始考试](https://images.gitee.com/uploads/images/2022/0212/170019_a3b5d719_10390001.png "屏幕截图.png")
![学生查看历史信息](https://images.gitee.com/uploads/images/2022/0212/170210_0bfe2e0b_10390001.png "屏幕截图.png")
#### 联系方式
1.  CSDN 地址 https://blog.csdn.net/LYXlyxll
2.  微信:iostreamX64
3.  QQ:846581636